import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarWarCharacterDetailComponent } from './star-war-character-detail.component';

describe('StarWarCharacterDetailComponent', () => {
  let component: StarWarCharacterDetailComponent;
  let fixture: ComponentFixture<StarWarCharacterDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarWarCharacterDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarWarCharacterDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
