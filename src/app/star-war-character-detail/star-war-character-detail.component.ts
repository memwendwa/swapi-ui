import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-star-war-character-detail',
  templateUrl: './star-war-character-detail.component.html',
  styleUrls: ['./star-war-character-detail.component.scss']
})
export class StarWarCharacterDetailComponent implements OnInit {

  @Input() character: any;
  constructor() { }

  ngOnInit() {
  }

}
