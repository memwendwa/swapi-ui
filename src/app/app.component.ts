import { Component, OnInit } from '@angular/core';
import { StarWarCharacterService } from './services/star-war-character.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  title = 'swapi-ui';
  private items$ = new BehaviorSubject<any[]>([]);
  selectedCharacter: any=null;
  constructor(private service: StarWarCharacterService){

  }

  ngOnInit(): void {
    this.service.getAll().subscribe(data=>{
      //console.log(data.results);
      data.results[0].isFavorite=true;
      this.items$.next(data.results);
    })
  }
  onSelectCharacter(character){
    this.selectedCharacter = character;
    console.log(character);
  }
}
