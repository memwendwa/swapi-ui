import { TestBed } from '@angular/core/testing';

import { StarWarCharacterService } from './star-war-character.service';

describe('StarWarCharacterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StarWarCharacterService = TestBed.get(StarWarCharacterService);
    expect(service).toBeTruthy();
  });
});
